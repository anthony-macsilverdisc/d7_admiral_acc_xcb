<?php
$name='DancingScript';
$type='TTF';
$desc=array (
  'Ascent' => 920,
  'Descent' => -280,
  'CapHeight' => 720,
  'Flags' => 4,
  'FontBBox' => '[-239 -280 1000 920]',
  'ItalicAngle' => 0,
  'StemV' => 87,
  'MissingWidth' => 278,
);
$up=-75;
$ut=50;
$ttffile='/usr/local/apache/virtualhosts/drupal7_generic/drupal7_accdev/sites/all/libraries/mpdf/ttfonts/Dancing_Script.ttf';
$TTCfontID='0';
$originalsize=116580;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='font1';
$panose=' a 2 3 8 6 0 4 5 7 0 d 0';
$haskerninfo=false;
$unAGlyphs=false;
?>