(function ($) {

    // Add a spinner on quantity widget.
    Drupal.behaviors.quantityWidgetSpinner = {
        attach: function (context, settings) {
            $('.form-item-quantity input').spinner({
                min: 25,
                max: 9999,
                increment: 'fast'
            });
        }
    }

    Drupal.behaviors.preventLettersStepTwo = {
        attach: function (context, settings) {
            $('.quan-step-two', context).jStepper({
                minValue:25,
                maxValue:9999,
                minLength:2
            });
        }
    }



    //WYSIWYG Behaviours
    Drupal.behaviors.wysiwyg = {
        attach: function (context, settings) {
            //Functions to hide and show the WYSIWYG Editor Toolbards and remove the text area labels when the ckeditors are selected.
            $('.form-textarea-wrapper').unbind().click(function () {
                var thisID = $(this).find('.cke_top').attr('id');
                $('#' + thisID).show('slow');
                $(this).siblings('label').css('display', 'none');
            });
            $('.form-textarea-wrapper').blur(function () {
                var thisID = $(this).find('.cke_top').attr('id');
                $('#' + thisID).hide('slow');
            });
        }
    }

    //Add toggle functionality for help button
    Drupal.behaviors.toggleCartError = {
        attach: function (context, settings) {
            $('.messages--error', context).unbind().click(function () {
                $('.messages--error').hide();
            });
        }
    };






    /* MB - Reset the quantity to 25 if less*/

    /**
     * Proce caclculator
     * @type {{attach: attach}}
     */
    Drupal.behaviors.pricecalc = {
        attach: function (context, settings) {


            if ($('.cost-price').html()) {

                var fullColor = 0.2; //Full Color Net of VAT Flat value 20 pence in £'s
                var vatValue = 1.2; //Current UK VAT
                var globalDiscount = 1; //Site wide global discount percentage 1 = off


                var breakQty = $('input:hidden[name=pricing_breakpoint]').val();
                breakQty = parseFloat(breakQty);
                var breakPrice = $('input:hidden[name=breakpoint_price]').val();// assumed to be net of VAT
                breakPrice = parseFloat(breakPrice);

                //Detect changes to the qty fields & operate on the values
                $('.edit-quan-box').unbind().change(function () {
                    updatePricing();
                });
                $('.form-item-field-inside-printed-colour-und select').unbind().change(function () {
                    updatePricing();
                });


                function colorCheck() {
                    if ($('.form-item-field-inside-printed-colour-und select').val() == 169 && $('.form-item-field-inside-printed-colour-und').html()) {
                        //alert(fullColor);

                        //alert(fullColor);
                        return fullColor;

                    } else {
                       //alert("B&W 1");
                        return 0;

                    }
                };

                function updatePricing() {
                    //alert("Function Running");


                    // Check the quantity before we start working out the price and set to 25 if too low
                    if($('.edit-quan-box').val() < 25){
                        $('.edit-quan-box').val(25);
                    }

                    var cardTotal = 0;
                    //Get initial card cost price
                    var cardCost = $('.cost-price').html(); // assumed to be net of VAT
                    cardCost = parseFloat(cardCost);
                    // alert(cardCost);

                    var cardQty = $('.edit-quan-box').val();
                    //alert(cardQty);
                    cardQty = parseFloat(cardQty);

                    if (cardQty < breakQty) {
                       // alert(colCost);
                        cardTotal = (cardCost + colorCheck()) * cardQty;
                       // alert("Total : " + cardTotal);
                    } else {
                        var price1 = breakQty * (cardCost + colorCheck());
                        var price2 = (cardQty - breakQty) * (breakPrice + colorCheck());
                        cardTotal = (price1 + price2);
                    }

                    //alert(cardTotal);

                    cardTotalEx = Math.round((cardTotal * globalDiscount) * 100) / 100; // Net of VAT
                    cardTotalInc = Math.round((cardTotalEx * vatValue) * 100) / 100; // Gross of VAT

                    //Take calculated values and re-insert the previously removed html before printing to screen
                    $('#edit-total-price-exvat .total-price').html(function () {
                        cardTotalEx = FormatMoney(cardTotalEx, '£', ' ex VAT', ',', '.', 2, 2);
                        return cardTotalEx;
                    });
                    $('#edit-total-price .total-price').html(function () {
                        cardTotalInc = FormatMoney(cardTotalInc, '£', ' inc VAT', ',', '.', 2, 2);
                        return cardTotalInc;
                    });

                    $('.total-price-exvat').html(function () {
                        cardTotalEx = FormatMoney(cardTotalEx, '£', ' ex VAT', ',', '.', 2, 2);
                        return cardTotalEx;
                    });
                    $('.total-price-inc-vat').html(function () {
                        cardTotalInc = FormatMoney(cardTotalInc, '£', ' inc VAT', ',', '.', 2, 2);
                        return cardTotalInc;
                    });


                }





            }
        }
    };

    //Prevent letters being typed in
    /*Drupal.behaviors.preventLetters = {
        attach: function (context, settings) {
            $('.form-item-quantity input', context).jStepper({minValue:25, maxValue:9999, minLength:2});
        }
    };*/

    //Add toggle functionality for help button
    Drupal.behaviors.toggleHelp = {
        attach: function (context, settings) {
            $('.toggle-help-icon', context).click(function () {
                $('.toggle-info').toggle('slow');
            });
        }
    };

    Drupal.behaviors.hidePreview = {
        attach: function (context, settings) {
            if ($('.messages--error').html()) {
                $('.super-duper-preview').hide();
            }
            // else{
            //    $('.super-duper-preview').show();
            // }
            $('.close-preview', context).click(function () {
                $('.super-duper-preview').hide();
            });
        }
    };






})(jQuery);


// FormatMoney() - returns a string represenation of the numeric value in _amount_ using the given formatting & currency settings.
//
// Preconditions:
//		display_after_decimal_pt >= 0
//
// Notes:
//		- Usually you'll want significant_after_decimal_pt to be less than or equal to display_after_decimal_pt.
//		- The returned value is automatically rounded to the nearest significant digit.
//		- An invalid (NaN) input amount is replaced by 0 (zero).
//		- If the combined number of visible digits would exceed 21, the digits are replaced by "#".
//		- Negative amounts are fully supported; the (optional) currency symbol is prefixed by a minus.
function FormatMoney(amount, currency_symbol_before, currency_symbol_after, thousands_separator, decimal_point, significant_after_decimal_pt, display_after_decimal_pt) {
    // 30JUL2008 MSPW  Fixed minus display by moving this line to the top
    // We need to know how the significant digits will alter our displayed number
    var significant_multiplier = Math.pow(10, significant_after_decimal_pt);

    // Only display a minus if the final displayed value is going to be <= -0.01 (or equivalent)
    var str_minus = (amount * significant_multiplier <= -0.5 ? "-" : "");

    // Sanity check on the incoming amount value
    amount = parseFloat(amount);
    if (isNaN(amount) || Math.LOG10E * Math.log(Math.abs(amount)) +
        Math.max(display_after_decimal_pt, significant_after_decimal_pt) >= 21) {
        return str_minus + currency_symbol_before +
            (isNaN(amount) ? "#" : "####################".substring(0, Math.LOG10E * Math.log(Math.abs(amount)))) +
            (display_after_decimal_pt >= 1 ?
                (decimal_point + "##################".substring(0, display_after_decimal_pt)) : "") +
            currency_symbol_after;
    }

    // Make +ve and ensure we round up/down properly later by adding half a penny now.
    amount = Math.abs(amount) + (0.5 / significant_multiplier);

    amount *= significant_multiplier;

    var str_display = parseInt(
        parseInt(amount) * Math.pow(10, display_after_decimal_pt - significant_after_decimal_pt)).toString();

    // Prefix as many zeroes as is necessary and strip the leading 1
    if (str_display.length <= display_after_decimal_pt)
        str_display = (Math.pow(10, display_after_decimal_pt - str_display.length + 1).toString() +
            str_display).substring(1);

    var comma_sep_pounds = str_display.substring(0, str_display.length - display_after_decimal_pt);
    var str_pence = str_display.substring(str_display.length - display_after_decimal_pt);

    if (thousands_separator.length > 0 && comma_sep_pounds.length > 3) {
        comma_sep_pounds += ",";

        // We need to do this twice because the first time only inserts half the commas.  The reason is
        // the part of the lookahead ([0-9]{3})+ also consumes characters; embedding one lookahead (?=...)
        // within another doesn't seem to work, so (?=[0-9](?=[0-9]{3})+,)(.)(...) fails to match anything.
        if (comma_sep_pounds.length > 7)
            comma_sep_pounds = comma_sep_pounds.replace(/(?=[0-9]([0-9]{3})+,)(.)(...)/g, "$2,$3");

        comma_sep_pounds = comma_sep_pounds.replace(/(?=[0-9]([0-9]{3})+,)(.)(...)/g, "$2,$3");

        // Remove the fake separator at the end, then replace all commas with the actual separator
        comma_sep_pounds = comma_sep_pounds.substring(0, comma_sep_pounds.length - 1).replace(/,/g, thousands_separator);
    }

    return str_minus + currency_symbol_before +
        comma_sep_pounds + (display_after_decimal_pt >= 1 ? (decimal_point + str_pence) : "") +
        currency_symbol_after;
}
