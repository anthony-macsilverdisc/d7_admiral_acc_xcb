<?php

/**
 * Implements hook_commerce_payment_method_info().
 */
function admiral_checkout_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['admiral_invoice_payment'] = array(
    'title' => t('Pay by invoice'),
    'description' => t('Demonstrates credit card payment during checkout and serves as a development example.'),
    'active' => TRUE,
  );

  return $payment_methods;
}

/**
 * Payment method callback: submit form.
 */
function admiral_checkout_submit_form($payment_method, $pane_values, $checkout_pane, $order) {
  return;
}

/**
 * Payment method callback: submit form validation.
 */
function admiral_checkout_submit_form_validate($payment_method, $pane_form, $pane_values, $order, $form_parents = array()) {
  // Validate the credit card fields.
}

/**
 * Payment method callback: submit form submission.
 */
function admiral_checkout_submit_form_submit($payment_method, $pane_form, $pane_values, $order, $charge) {
  // $order->data['commerce_kickstart_payment'] = $pane_values;

  admiral_checkout_transaction($payment_method, $order, $charge);
}

/**
 * Creates an example payment transaction for the specified charge amount.
 *
 * @param $payment_method
 *   The payment method instance object used to charge this payment.
 * @param $order
 *   The order object the payment applies to.
 * @param $charge
 *   An array indicating the amount and currency code to charge.
 * @param $name
 *   The name entered on the submission form.
 */
function admiral_checkout_transaction($payment_method, $order, $charge) {
  $transaction = commerce_payment_transaction_new('admiral_invoice_payment', $order->order_id);
  $transaction->instance_id = $payment_method['instance_id'];
  $transaction->amount = $charge['amount'];
  $transaction->currency_code = $charge['currency_code'];
  $transaction->status = COMMERCE_PAYMENT_STATUS_SUCCESS;
  $transaction->message = 'Name: @name';
  $transaction->message_variables = array('@name' => 'Admiral : Pay by Invoice');

  commerce_payment_transaction_save($transaction);
  return $transaction;
}
